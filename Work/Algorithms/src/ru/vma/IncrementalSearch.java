package ru.vma;

import java.util.Scanner;
import java.util.ArrayList;

public class IncrementalSearch {


    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(33);
        numbers.add(45);
        numbers.add(238);
        numbers.add(6);
        numbers.add(68);
        numbers.add(1234);
        numbers.add(0);
        numbers.add(3);
        numbers.add(7);
        numbers.add(8);
        numbers.add(25);

        int[] numb = {33, 34, 45, 238, 6, 68, 1234, 0, 3, 7, 8, 25};
        System.out.print("Введите число для поиска: ");
        int num = scanner.nextInt();

        System.out.println("Массив чисел:");
        OutputMassive(numb);
        System.out.println();
        if (linearSearch(numb, num) == -1) {
            System.out.println("элемент " + num + " не найден");
        } else {
            System.out.println("элемент " + num + " найден по индексу " + linearSearch(numb, num));
        }

        System.out.println("ArrayList чисел:");
        OutputArray(numbers);
        System.out.println();
        if (SequentialArraySearch(numbers, num) == -1) {
            System.out.println("элемент " + num + " не найден");
        } else {
            System.out.println("элемент " + num + " найден по индексу " + SequentialArraySearch(numbers, num));
        }
    }

    private static int linearSearch(int[] arr, int elementToSearch) {

        for (int index = 0; index < arr.length; index++) {
            if (arr[index] == elementToSearch)
                return index;
        }
        return -1;
    }

    private static void OutputMassive(int[] arr) {
        for (int value : arr) {
            System.out.print(value + " ");
        }
    }

    private static void OutputArray(ArrayList<Integer> numbers) {
        for (Integer number : numbers) {
            System.out.print(number + " ");
        }
    }

    private static int SequentialArraySearch(ArrayList<Integer> numbers, int num) {
        int size = numbers.size();
        for (int index = 0; index < size; index++) {
            if (numbers.get(index) == num) {
                return index;
            }
        }
        return -1;
    }
}

