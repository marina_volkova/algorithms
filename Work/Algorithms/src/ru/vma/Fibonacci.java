package ru.vma;

import java.math.BigInteger;
import java.util.Scanner;

public class Fibonacci {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        int amt = scanner.nextInt();
        long before = System.currentTimeMillis();
        if (amt <= 46) {
            System.out.println("Medium: " + fibonacci(amt));
            System.out.println("Long: " + longFibonacci(amt));
            System.out.println("Big: " + bigFibonacci(amt));
        }
        if (amt <= 92 && amt > 46) {
            System.out.println("Long: " + longFibonacci(amt));
            System.out.println("Big: " + bigFibonacci(amt));
        }
        if (amt > 92) {
            System.out.println("Big: " + bigFibonacci(amt));
        }
        long after = System.currentTimeMillis();
        System.out.println(after - before + " миллисекунд");

        long before1 = System.currentTimeMillis();
        System.out.println(fibonacciRec(amt) + "rec");
        long after1 = System.currentTimeMillis();
        System.out.println(after1 - before1 + " миллисекунд");
    }

    private static int fibonacci(int amt) {
        int first = 1;
        int second = 1;
        int sum = 0;
        if (amt == 1 || amt == 2) {
            return 1;
        }
        for (int i = 2; i < amt; i++) {
            sum = first + second;
            first = second;
            second = sum;
        }
        return sum;
    }

    private static long longFibonacci(int amt) {
        long first = 1;
        long second = 1;
        long sum = 0;
        if (amt == 1 || amt == 2) {
            return 1;
        }
        for (int i = 2; i < amt; i++) {
            sum = first + second;
            first = second;
            second = sum;
        }
        return sum;
    }

    private static BigInteger bigFibonacci(int amt) {
        BigInteger first = BigInteger.ONE;
        BigInteger second = BigInteger.ONE;
        BigInteger sum = BigInteger.ZERO;
        if (amt == 1 || amt == 2) {
            return BigInteger.ONE;
        }
        for (int i = 2; i < amt; i++) {
            sum = first.add(second);
            first = second;
            second = sum;
        }
        return sum;
    }

    private static int fibonacciRec(int amt) {

        if (amt == 0) {
            return 0;
        }
        if (amt == 1) {
            return 1;
        } else {
            return fibonacciRec(amt - 1) + fibonacciRec(amt - 2);
        }
    }
}
