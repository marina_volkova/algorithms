package ru.vma;

import java.util.ArrayList;
import java.util.Arrays;

public class SortingInserts {
    public static void main(String[] args) {
        int[] array = {5, 4, 1, 7, 3, 9, 2, 6, 8};
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(5, 4, 1, 7, 3, 9, 2, 6, 8));
        System.out.println("ArrayList: " + arrayList);
        insertSort(arrayList);
        System.out.println("Упорядоченный ArrayList: " + arrayList + "\n");

        System.out.println("Числовая последовательность: " + Arrays.toString(array));
        insertSort(array);
        System.out.println("Упорядоченная числовая последовательность: " + Arrays.toString(array));
    }

    private static void insertSort(ArrayList<Integer> arrayList) {
        for (int out = 1; out < arrayList.size(); out++) {
            int temp = arrayList.get(out);
            int in = out;
            while (in > 0 && arrayList.get(in - 1) <= temp) {
                arrayList.set(in, arrayList.get(in - 1));
                in--;
            }
            arrayList.set(in, temp);
        }
    }

    private static void insertSort(int[] array) {
        for (int out = 1; out < array.length; out++) {
            int temp = array[out];
            int in = out;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;
        }
    }
}
