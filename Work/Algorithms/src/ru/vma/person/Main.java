package ru.vma.person;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Person person1 = new Person("Б.в", 1999);
        Person person2 = new Person("А.б", 2000);
        Person person3 = new Person("В.г", 1958);

        Person[] people = {person1, person2, person3};
        out(people);
        sorting(people);
        System.out.println();
        out(people);
        System.out.println("\nsurname:");
        String person = scanner.nextLine();
        finding(people, person);
    }

    private static void sorting(Person[] people) {
        for (int i = 0; i < people.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < people.length; j++) {
                int isCompare = people[min].getSurname().compareTo(people[j].getSurname());
                if (isCompare > 0) {
                    min = j;
                }
            }
            if (i != min) {
                Person temp = people[i];
                people[i] = people[min];
                people[min] = temp;
            }
        }
    }


    private static void out(Person[] people) {
        for (Person person : people) {
            System.out.println(person + " ");
        }
    }


    private static void finding(Person[] people, String person) {
        boolean isNotHere = true;
        for (Person value : people) {
            if (person.equalsIgnoreCase(value.getSurname())) {
                System.out.println(value + " ");
                isNotHere = false;
            }
        }
        if (isNotHere){
            System.out.println("Такого нет");
        }
    }
}
