package ru.vma;

import java.util.Arrays;
import java.util.Scanner;

public class ExampleBinarySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
        System.out.print("Введите число для поиска:");
        int number = scanner.nextInt();

        System.out.println("До сортировки: " + Arrays.toString(array));
        Arrays.sort(array);
        System.out.println("После сортировки: " + Arrays.toString(array));

        binarySearch(array, number);
    }

    private static void binarySearch(int[] array, int number) {

        int first = 0;
        int last = array.length - 1;

        while (first <= last) {
            int mid = (first + last) / 2;

            int temp = array[mid];

            if (temp == number) {
                System.out.println("Число " + number + " найдено по индексу: " + last);
                return;
            }
            if (temp < number) {
                first = mid + 1;

            } else {
                last = mid - 1;
            }
        }
        System.out.println("Числа " + number + " нет");
    }
}