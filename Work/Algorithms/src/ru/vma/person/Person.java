package ru.vma.person;

public class Person {
    private  String surname;
    private  int year;

    Person(String surname, int year) {
        this.surname = surname;
        this.year = year;
    }

    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", year=" + year +
                '}';
    }

    String getSurname() {
        return surname;
    }

}
