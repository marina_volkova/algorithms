package ru.vma;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {5, 4, 1, 7, 3, 9, 2, 6, 8};
        System.out.println("Числовая последовательность: " + Arrays.toString(array) + "\n");
        bubbleSort(array);
        System.out.println("Упорядоченная числовая последовательность: " + Arrays.toString(array));
    }

    private static void bubbleSort(int[] array) {
    /*Внешний цикл каждый раз сокращает фрагмент массива,
      так как внутренний цикл каждый раз ставит в конец
      фрагмента максимальный элемент*/
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
            /*Сравниваем элементы попарно,
              если они имеют неправильный порядок,
              то меняем местами*/
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }
}
